# joinfediverse
* Discussion autour du projet de site vitrine du fediverse, accessible aux non-techniques.
https://framateam.org/ux-framatrucs/channels/joinfediverse
* Pad principal: https://mypads.framapad.org/p/portail-d-entree-de-la-federation-6m1n2v75j
* Logos :https://llaq-drive.mycozy.cloud/public?sharecode=0KkWTBzdYtzv 
* Groupe framagit :https://framagit.org/groups/join-fediverse-project/ 
* Maquettes :https://sketch.cloud/s/kR8el/v/KvanRg


## installation locale sur windows
à lancer dans un Git bash
disposer de [Git](https://git-scm.com/download/win) et [Npm](https://nodejs.org/en/download/) (livré avec NodeJS) sur sa machine.
```bash
git clone https://framagit.org/tykayn/joinfediverse.git
cd joinfediverse
npm i -g yarn
yarn install
yarn serve
```
ouvrir ensuite votre navigateur respectueux de la vie privée favori, par exemple Firefox à l'URL http://localhost:8080
``` 
http://localhost:8080
```
## Installation dans un système Ubuntu/debian
### Dépendances Node et Git

```bash
curl -L https://www.npmjs.com/install.sh | sh
sudo apt install git

git clone https://framagit.org/tykayn/joinfediverse.git
cd joinfediverse
npm i -g yarn
yarn install
yarn serve
```


# déploiement
il faut builder le projet et se monter un serveur. Demandez de l'aide à un CHATONS.


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Run your end-to-end tests
```
yarn test:e2e
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Optimize assets
use optipng for PNG pictures only
```
sudo apt install optipng
for i in *.png; do optipng -o5 -quiet -keep -preserve -dir optimized -lo ooptipng.log "$i"; done
```
for JPEG picture,
```
sudo apt-get install jpegoptim
for i in *.jpg; do jpegoptim -d ./optimized -p “$i”; done
```
