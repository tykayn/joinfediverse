import {expect} from 'chai';
import {shallowMount} from '@vue/test-utils';
import HelloWorld from '@/components/HelloWorld.vue';
import {RouterLinkStub} from '@vue/test-utils';

describe('HelloWorld.vue', () => {
  it('renders Fédiverse when passed', () => {
    const fedi = 'Fédiverse';
    const wrapper = shallowMount(HelloWorld, {
      stubs: ['router-link'],
      propsData: {fedi},
    });
    expect(wrapper.text()).to.include(fedi);
  });
});
