import Vue from 'vue';
import VueRouter, {RouteConfig, RouterOptions} from 'vue-router';
import Home from '@/views/Home.vue';
import Spheres from '@/components/spheres/Spheres.vue';
import Rejoindre from '@/components/Rejoindre.vue';
import Decouvrir from '@/components/Decouvrir.vue';
import Recap from '@/components/Recapitulatif.vue';
import DiscoverMicroblogging from '@/components/discover/microblogging/index.vue';
import Instance from '@/components/Instance.vue';
import DiscoverMusic from '@/components/discover/music/index.vue';
import DiscoverPhoto from '@/components/discover/photo/index.vue';
import DiscoverEvent from '@/components/discover/events/index.vue';
import DiscoverRedaction from '@/components/discover/redaction/index.vue';
import DiscoverVideo from '@/components/discover/video/index.vue';

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: '/',
    name: 'home',
    component: Home,
  }, {
    path: '/rejoindre',
    name: 'join',
    component: Rejoindre,
  }, {
    path: '/decouvrir',
    name: 'discover',
    props: true,
    component: Decouvrir,
    children: [
      {
        path: 'microblog',
        name: 'discover-microblog',
        component: DiscoverMicroblogging,
      }, {
        path: 'photo',
        name: 'discover-photo',
        component: DiscoverPhoto,
      }, {
        path: 'music',
        name: 'discover-music',
        component: DiscoverMusic,
      }, {
        path: 'events',
        name: 'discover-events',
        component: DiscoverEvent,
      }, {
        path: 'redaction',
        name: 'discover-redaction',
        component: DiscoverRedaction,
      },
      {
        path: 'video',
        name: 'discover-video',
        component: DiscoverVideo,
      },
    ],
  },
  {
    path: '/spheres',
    name: 'spheres',
    component: Spheres,
  },
  {
    path: '/instance',
    name: 'instances',
    component: Instance,
  }, {
    path: '/recap',
    name: 'recapitulatif',
    component: Recap,
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
];


const options: RouterOptions = {
  base: process.env.BASE_URL,
  mode: 'history',
  routes,
};

const router = new VueRouter(options);

export default router;
