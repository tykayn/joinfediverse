export const softwaresList = {
  mastodon: {
    title: 'mastodon',
    url: 'joinmastodon.org',
  },
  pleroma: {
    title: 'pleroma',
    url: 'pleroma.social',
  },
};

export interface InstanceDescription {
  title: string;
  langs: string;
  description: object;
  thumbnail?: string;
  url: string;
  stats?: object;
  contact_account?: object;
  registrations?: boolean;
}

/**
 * model:
 * {
 *       title: '',
 *       langs: 'fr',
 *       description: {
 *         fr: '',
 *         en: '',
 *       },
 *       url: '',
 *     },
 */

export const instancesList = {
  pleroma: [
    {
      title: 'Luciferist',
      langs: 'fr/en',
      description: {
        fr: 'Une instance Pleroma anarqueer et luciférienne, ouvertes à toutes et à tous',
        en: '',
      },
      url: 'soc.luciferi.st',
    }, {
      title: 'Blob Cat',
      langs: 'fr',
      description: {
        fr: '',
        en: '',
      },
      url: 'blob.cat',
    }, {
      title: 'Pleroma.fr',
      langs: 'fr',
      description: {
        fr: '',
        en: '',
      },
      url: 'pleroma.fr',
    },
  ],
  mastodon: [{
    title: 'Mastodon Tedomum',
    langs: 'fr',
    description: {
      fr: 'une instance proposée par une association qui défend le respect de la vie privée et les libertés individuelles ',
      en: '',
    },
    thumbnail: 'https://mastodon.tedomum.net/system/site_uploads/files/000/000/001/original/logo_small_high.png?1557595032',
    url: 'mastodon.tedomum.net',
  }, {
    title: 'Eldritch Café',
    langs: 'fr/en',
    description: {
      fr: 'Une instance se voulant accueillante pour les personnes queers, féministes et anarchistes ainsi que pour leurs sympathisant·e·s.',
      en: 'A welcoming instance for queer, feminist and anarchist people as well as their sympathizers. We are mainly French-speaking people, but you are welcome whatever your language might be',
    },
    url: 'eldritch.cafe',
  }, {
    title: 'toot.aquilenet.fr',
    langs: 'fr',
    description: {
      fr: 'Proposée par l\'association Aquilenet qui défent la culture d\'un Internet local et respectueux du principe des communs, du partage, ouvert et neutre.',
      en: '',
    },
    url: 'toot.aquilenet.fr',
  }, {
    title: 'Mastodon Eric',
    langs: 'fr/en',
    description: {
      fr: 'Une instance ouverte par un particulier qui souhaite sortir des géants du web',
      en: '',
    },
    url: 'mastodon.eric.ovh',
  }, {
    title: 'Diaspodon.fr',
    langs: 'fr',
    description: {
      fr: 'Une instance ouverte par un blogueur qui souhaite aider à sortir des géants du web',
      en: '',
    },
    url: 'diaspodon.fr',
  }, {
    title: 'Mastodon Parti Pirate',
    langs: 'fr/en',
    description: {
      fr: 'L\'instance du Parti Pirate de Belgique, une organisation politique qui lutte pour la démocratie, la transparence, les logiciels libres, et une réforme du droit d\'auteur, entre autres.',
      en: '',
    },
    url: 'mastodon.pirateparty.be',
  }, {
    title: 'Tooting.ch',
    langs: 'fr/en',
    description: {
      fr: 'Une instance généraliste hébergée par l\'association FairSocialNet',
      en: 'Generic Mastodon instance hosted by the FairSocialNet association',
    },
    url: 'tooting.ch',
  },
    {
      title: 'Biscuit Town',
      langs: 'fr',
      description: {
        fr: 'Biscuit.town est une instance par et pour les personnes neuroqueer, féministes et gauchistes.',
        en: '',
      },
      url: 'biscuit.town',
    },
    {
      title: 'Mastodon Cipherbliss',
      langs: 'fr/en',
      description: {
        fr: 'La meilleure instance du fédiverse (rien que ça), généraliste, inclusive, et aux fonctionnalités expérimentales telles qu\'une messagerie instantanée et 7777 caractères par post.',
        en: 'Best instance in the fediverse (nothing less), generalist, inclusive, providing experimental functionnalities like instant messaging and 7777 chars per post.',
      },
      url: 'mastodon.cipherbliss.com',
    },
    {
      title: 'travelpandas.fr',
      langs: 'fr/en',
      description: {
        fr: 'Une petite instance généraliste, ouverte à toutes et à tous !',
        en: '',
      },
      url: 'travelpandas.fr',
      thumbnail: 'https://media.travelpandas.fr/mastodon/site_uploads/files/000/000/003/original/pandarouxmechant.png',
    }],
  plume: [{
    title: 'Fédiverse Blog',
    langs: 'fr',
    description: {
      fr: 'une instance généraliste et multilingue, où on peut retrouver des blogs sur tous les thèmes',
      en: '',
    },
    url: 'fediverse.blog',
  }, {
    title: 'Plume Eric',
    langs: 'fr',
    description: {
      fr: 'Une instance ouverte par un particulier qui souhaite sortir des géants du web ',
      en: '',
    },
    url: 'plume.eric.ovh',
  }],
  pixelfed: [{
    title: 'Pixelfed Eric',
    langs: 'fr',
    description: {
      fr: 'Une instance ouverte par un particulier qui souhaite sortir des géants du web',
      en: '',
    },
    url: 'pixelfed.eric.ovh',
  }],
  peertube: [{
    title: 'Peertube Bliss',
    langs: 'fr',
    description: {
      fr: 'Peertube généraliste avec 1 giga d\'hébergement offert pour les vidéastes en tout genre qui veulent toucher un nouveau public',
      en: '',
    },
    url: 'peertube.cipherbliss.com',
  }, {
    title: 'Hitchtube',
    langs: 'fr',
    description: {
      fr: '    Hitchtube a pour but de réunir des vidéos concernant l\'autostop, le voyage sans argent et toutes formes alternatives de voyages, respectueuses de l\'environnement et des humains.',
      en: '',
    },
    url: 'hitchtube.fr',
  }, {
    title: 'Peertube Iselfhost',
    langs: 'fr',
    description: {
      fr: 'Une instance généraliste, proposée par un particulier, ouverte à toutes et à tous',
      en: '',
    },
    url: 'peertube.iselfhost.com',
  }, {
    title: 'Peertube Eric',
    langs: 'fr',
    description: {
      fr: 'Une instance ouverte par un particulier qui souhaite sortir des géants du web',
      en: '',
    },
    url: 'peertube.eric.ovh',
  },
    {
      title: 'Pair 2 jeux',
      langs: 'fr',
      description: {
        fr: 'regroupant des créateurs de vidéos de jeux vidéos de tout type et de toutes origines. Attention, si vous êtes vidéaste, il n\'y a plus de place pour y déposer de nouvelles vidéos !',
        en: '',
      },
      url: 'videos.pair2jeux.tube/',
    }],
  funkwhale: [{
    title: 'Tanuki Tunes',
    langs: 'fr',
    description: {
      fr: '',
      en: '',
    },
    url: 'tanukitunes.com',
  }, {
    title: 'Open audio',
    langs: 'fr',
    description: {
      fr: '',
      en: '',
    },
    url: 'open.audio',
  }, {
    title: '/!\Music Cipherbliss',
    langs: 'fr',
    description: {
      fr: '',
      en: '',
    },
    url: 'music.cipherbliss.com',
  }],
  mobilizon: [{
    title: 'Test Mobilizon',
    langs: 'fr',
    description: {
      fr: 'Ceci est un site de démonstration permettant de tester la version bêta de Mobilizon. Merci de ne pas en faire une utilisation réelle.\n' +
        '\n' +
        'Mobilizon est en cours de développement, nous ajouterons de nouvelles fonctionnalités à ce site lors de mises à jour régulières, jusqu\'à la publication de la version 1 du logiciel au premier semestre 2020.D\'ici là, veuillez considérer que le logiciel n\'est pas (encore) fini. ',
      en: '',
    },
    url: 'test.mobilizon.org',
  }],
  writefreely: [{
    title: 'WordSmith.social',
    langs: 'fr',
    description: {
      fr: 'WordSmith',
      en: 'WordSmith',
    },
    url: 'wordsmith.social',
  },
    {
      title: 'personal journal.ca',
      langs: 'fr',
      description: {
        fr: '',
        en: '',
      },
      url: 'personaljournal.ca',
    }],
};
